package com.company;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Stack stack = new Stack();
        Stack stackEmpty = new Stack();
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stackEmpty.pop());
        stack.push("test");
        stack.push("test1");
        stack.push("test2");
        stack.push("10");
        stack.push("20");
        stack.push("30");
        stack.push("40");
        stack.push("50");
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stackEmpty.pop());
        stack.push("1");
        stack.push("2");
        stack.push("3");
        stack.push("4");
        stack.push("5");
        System.out.println("\n");
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stackEmpty.pop());

    }
}
