package com.company;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Stack implements StackOperations {

    List<String> stack = new LinkedList<>();

    @Override
    public List<String> get() {
        if(stack.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        else{
            return stack;
        }
    }

    @Override
    public Optional<String> pop() {
        if(stack.isEmpty()){
            return Optional.empty();
        }
        else{
            String item=stack.get(0);
            stack.remove(0);
            return Optional.of(item);
        }
    }

    @Override
    public void push(String item) {
        stack.add(0,item);
    }
}
